import React, { Component } from 'react';
import myData from './operators.json';
import _ from 'lodash';
const MyContext = React.createContext();
class MyProvider extends Component {
    state = {
        name: 'Wes',
        age: 100,
        cool: true,
        opertors: [],
        opertorData: myData,
        tableRows: [],
        message: '',
        showMessage: false
    }
    assignValues = (datas) => {
        let nonNullData;
        nonNullData = _.map(datas, function (o) {
            if (o.operatorname != '' && o.numberprefix != '' && o.cost != '') return o;
        });
        nonNullData = _.without(nonNullData, undefined);
        nonNullData.forEach(function (item, key) {
            if (this.isDuplicate(item.operatorname, item.numberprefix) == false) {
                this.state.opertorData.push(item);
              
            }
        }, this);
        this.setState({ message: '', showMessage: true });
        let msg = 'New opertor values are saved succesfully';
        this.setState({ message: msg, showMessage: true });
    }
    searchOperators = (searchText) => {
        this.setState({ message: '', showMessage: true });
        if (searchText == '') {
            let msg = 'Please enter telephone number';
            this.setState({ message: msg, showMessage: true });
            return;
        }
        let data = this.exactMatch(searchText);
        if (data.length == 1) {
            let msg = 'The operator is : ' + data[0].operatorname + ' and cost is : ' + data[0].cost;
            this.setState({ message: msg, showMessage: true });
        }
        else if (data.length > 1) {
            let leastData = this.findLeastOpertor(data);
            let msg = 'The operator is : ' + leastData.operatorname + ' and cost is : ' + leastData.cost;
            this.setState({ message: msg, showMessage: true });
        }
        else {
            let str = searchText.replace(/[^a-zA-Z0-9]/g, '');
            var regex = '/' + str + '/g';
            let exactData = [];
            const filtered = _.filter(this.state.opertorData, function (data) {
                if (data.numberprefix.toString().lastIndexOf(str, 0) === 0){
                    exactData.push(data);
                }
            });
            if (exactData.length == 1) {
                let msg = 'The operator is : ' + exactData[0].operatorname + ' and cost is : ' + exactData[0].cost;
                this.setState({ message: msg, showMessage: true });
            }
            else if (exactData.length > 1) {
                let leastData = this.findLeastOpertor(exactData);
                let msg = 'The operator is : ' + leastData.operatorname + ' and cost is : ' + leastData.cost;
                this.setState({ message: msg, showMessage: true });
            }
            else {
                exactData = [];
                const filtered = _.filter(this.state.opertorData, function (data) {
                    if (data.numberprefix.length < str.length && str.toString().startsWith(data.numberprefix.toString())) {
                        exactData.push(data);
                    }
                });
                if (exactData.length == 1) {
                    let msg = 'The operator is : ' + exactData[0].operatorname + ' and cost is : ' + exactData[0].cost;
                    this.setState({ message: msg, showMessage: true });
                }
                else if (exactData.length > 1) {
                    let leastData = this.findLeastOpertor(exactData);
                    let msg = 'The operator is : ' + leastData.operatorname + ' and cost is : ' + leastData.cost;
                    this.setState({ message: msg, showMessage: true });
                }
                else {
                    let msg = 'No matching operator found';
                    this.setState({ message: msg, showMessage: true });
                }
        }
        }
    }

      startsWithVar = (str,word) => {
          return str.lastIndexOf(word, 0) === 0;
      }
  findLeastOpertor = (obj) => {

      let minimum = obj.reduce(function (prev, curr) {
          return prev.cost < curr.cost ? prev : curr;
      });
      return minimum;
  }
  exactMatch = (searchText) => {
      let data = _.filter(this.state.opertorData, function (o) {
          return o.numberprefix == searchText;
      })
      if (data.length > 0) {
          return data
      }
      else {
          return [];
      }
  }
  isDuplicate = (operatorname, numberprefix) => {
     let data = _.filter(this.state.opertorData, function (o) {
         return o.operatorname == operatorname && o.numberprefix == numberprefix;
      })
     if (data.length > 0) {
         return true
     }
     else {
         return false;
     }
  }
  
  render() {
    return (
      <MyContext.Provider value={{
        state: this.state,
        growAYearOlder: () => this.setState({
          age: this.state.age + 1
        }),
        assignValues: (datas) => this.assignValues(datas),
        searchOperators: (searchText) => this.searchOperators(searchText)
      }}>
        {this.props.children}
      </MyContext.Provider>
    )
  }
}

const Family = (props) => (
  <div className="family">
    <Person />
  </div>
)

class Search extends Component {
    state = {
        teleponeNumber: ''
    }
    setTextValue = (event) => {
        let value = event.target.value;
        this.setState({teleponeNumber:value});
    }
    render() {
        return (
            <div className="person">
                <MyContext.Consumer>
                    {(context) => (
                        <div>
                        <div class="row">
                            <div class="col-25">
                                    <label>Telphone Number:</label>
                            </div>
                            <div class="col-25">
                                    <input type="text" value={this.state.teleponeNumber} onChange={(e) => {
                                        this.setTextValue(e)
                                    }} id="fname" name="firstname" placeholder="Please enter telphone number.."/>
                            </div>
                            <div class="col-25">
                                    <input type="submit" value="Search" onClick={(e) => {
                                        context.searchOperators(this.state.teleponeNumber)
                                    }}/>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-25">
                                    <label></label>
                                </div>
                                <div class="col-25">
                                    <label style={{'color':'Red'}}>{context.state.message}</label>
                                </div>
                       </div>
                    </div> 
                        )}
                </MyContext.Consumer>
            </div>
        )
    }
}

class TableComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { size: 3, rows: [] }
        var arry = { 'operatorname': '', 'numberprefix': '', 'cost': ''};
        this.state.rows.push(arry);
    }
   saveOperators = () => {
       let operators = this.state.rows;
       this.context.assignValues(operators);
   }
    addRow = () => {
        var arry = {"operatorname": '',"numberprefix": '',"cost": '' };
        let conctArray = [];
        conctArray = this.state.rows;
        conctArray.push(arry);
        this.setState({ rows: conctArray });
   }
    setTextValue = (indx, operator, event) => {
        let value = event.target.value;
        let conctArray = [];
        conctArray = this.state.rows;
        conctArray[indx][operator] = value;
        this.setState({ rows: conctArray });
    }
    render() {
        return (
            <MyContext.Consumer>
                {(context) => (
                    <div>
                    <div class="row">
                        <div class="col-25">
                            <label for="fname"></label>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-25">
                                <label for="fname"></label>
                            </div>
                            <div class="col-25">
                                <input type="submit" value="Save Operator" onClick={(e) => {
                                    context.assignValues(this.state.rows)
                                }} />
                                <div class="col-25" >
                                    <input type="submit" class="addRow" value="Add Rows" onClick={this.addRow} />
                                </div>
                            </div>
                                    
                                </div>
                    <div class="row">
                            <div class="col-25">
                                <label for="fname"></label>
                            </div>
                                    <div class="col-25" >
                                <div class="table-responsive">
                     <table class="table" id="tableOperator">
                        <thead>
                             <tr>
                                <th>Operator Name</th>
                                <th>Number Prefix</th>
                                <th>Cost</th>
                            </tr>
                        </thead>
                        <tbody>
                                        {this.state.rows.map(function (nextItem, j) {
                                            return (
                                                <tr key={j}>
                                                    <td><input class="col-sm-12 col-md-4 form-control" type="text" size="30" id="latbox" value={nextItem.operatorname} onChange={(e) => {
                                                        this.setTextValue(j,'operatorname',e)
                                                    }}/></td>
                                                    <td><input class="col-sm-12 col-md-4 form-control" type="number" size="30" id="latbox" value={nextItem.numberprefix} onChange={(e) => {
                                                        this.setTextValue(j, 'numberprefix', e)
                                                    }} /></td>
                                                    <td><input class="col-sm-12 col-md-4 form-control" type="number" size="30" id="latbox" value={nextItem.cost} onChange={(e) => {
                                                        this.setTextValue(j, 'cost', e)
                                                    }}/></td>
                                                </tr>
                                            )
                                        }, this)}
                        </tbody>
                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </MyContext.Consumer>
        )
    }
}

class Person extends Component {
  render() {
    return (
      <div className="person">
        <MyContext.Consumer>
          {(context) => (
            <React.Fragment>
              <p>Age: {context.state.age}</p>
              <p>Name: {context.state.name}</p>
              <button onClick={context.growAYearOlder}>🍰🍥🎂</button>
            </React.Fragment>
          )}
        </MyContext.Consumer>
      </div>
    )
  }
}


class App extends Component {
  render() {
    return (
        <MyProvider>
            <div class="row">
            <div class="col-25">
                <label for="fname"></label>
            </div>
            <h1>Routing Of Telephone Calls</h1>
          <Search />
          <TableComponent/>
        </div>
      </MyProvider>
    );
  }
}


export default App;
